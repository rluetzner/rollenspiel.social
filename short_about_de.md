rollenspiel.social ist eine Mastodon-Instanz für Rollenspieler und wird von der Rollenspiel-Community <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> bereitgestellt. Wir bieten einen platz für Rollenspiel, Pen & Paper, Tabletop, TCG und vieles mehr.
Die primären Instanzensprachen sind Deutsch.
