[English](/about/more#guidelines) version below</p>

## Richtlinien

Auf rollenspiel.social ist unsere Hauptrichtlinie:  
→ "Seid nett zueinander!"

> Leider ist das nicht immer so einfach wie es klingt, daher haben wir hier ein paar Punkte zusammen gefasst um dir einen Einblick in unsere Moderationsrichtlinien zu geben. Die endgültige Auslegung obliegt der Verwaltung von rollenspiel.social. Die Verwaltung behält sich das Recht vor, Toots zu löschen oder einen Account zu Blockieren wenn die Probleme bestehen.

Denk daran, dass es auf der anderen Seite des Bildschirms echte Menschen gibt und diese Menschen können andere Gefühle, Meinungen und Erfahrungen haben als du. Einige von ihnen sind vielleicht müde oder haben einen schlechten Tag.

Du musst nicht mit jemandem, der gleichen Meinung sein, um höflich zu sein. Wenn du nicht zivilisiert mit jemandem umgehen kannst, lass ihn in Ruhe.

Wenn du nicht mit jemandem interagieren möchtest oder einen seiner Beiträge nicht sehen möchtest, stehen dir Tools wie Mute- und Blockfunktionen zur Verfügung.

Wenn dich die Beiträge von jemandem wütend machen oder verärgern, nimm dir etwas Zeit, um dich zu beruhigen, bevor du antwortest. Du könntest auch versuchen sie zu bitten, dich nicht mehr zu erwähnen, wenn du ein Thema nicht mehr diskutieren möchtest.

Denke daran, dass du das Verhalten anderer Menschen nicht kontrollieren kannst, aber dennoch für dein eigenes Verhalten verantwortlich bist. Auch wenn du das Gefühl hast, dass jemand unhöflich zu dir oder jemand anderem war.

Wir erlauben kein missbräuchliches oder störendes Verhalten von oder gegenüber Nutzern auf rollenspiel.social. Dazu gehört auch Leute zu kontaktieren, die klargemacht haben, dass sie keinen Kontakt wollen; sowie andere Benutzer dazu anzuregen, in deinem Namen mit dieser Person in Kontakt zu treten. Die weitergab personenbezogenen Daten einer anderen Person ohne deren Zustimmung.

Bevor du etwas veröffentlichst, denke darüber nach, ob dies etwas ist das du gerne in der öffentlichen Zeitleiste sehen möchtest (oder dass deine Mutter oder deine Kinder es sehen). Es gibt Einstellungen für die Sichtbarkeit, in welchen Zeitachsen dein Beitrag erscheint. Nutze auch die Schaltflächen CW um bestimmte Texte und Bilder in deinem Beitrag auszublenden.

Wenn du überwiegend automatische Beiträge veröffentlichst, kennzeichne dein Account bitte als Bot. Wenn es mehr als fünf Beiträge am Tag sind, veröffentliche deine Beiträge als "Nicht gelistet(unlisted)"; damit in der lokalen Zeitleiste auch andere Beiträge zu finden sind.

Crossposting aus Twitter bitte ist nur als "Nicht Gelistet" oder noch besser crosspostet von Mastodon zu Twitter.

Die Instanzsprache ist Deutsch, daher bitten wir alle User die Beiträge auch auf Deutsch zu veröffentlichen. Abseits davon können Beiträge auch in Englisch verfasst werden, Beiträge oder Konten(Profiltexte oder Links auf Webseiten) in anderen Sprachen können von uns nicht moderiert werden, daher werden wir diese Entfernen.

### Diese Inhalte auf rollenspiel.social sind nicht erlaubt.

- Sexuelle Darstellungen von Kindern
- In Deutschland illegale Inhalte
- Diskriminierung von Geschlechtern und sexuellen Minderheiten oder deren Befürwortung
- Gewalttätige nationalistische Propaganda, Nazi-Symbolismus oder Förderung der Ideologie des Nationalsozialismus

### Inhalte, die entfernt werden wenn sie in der öffentlichen Zeitleiste ohne Kennzeichnung auftauchen:

- Werbung
- Pornografische und/oder sexueller Inhalte
- Gewaltdarstellungen in Wort und Bild
- Religiöse & politische Inhalte

> Mit Kennzeichnung ist die CW([content warnings](https://handbuch.rollenspiel.monster/mastodon/toots/content-warning.html)) Funktion gemeint.

### Account die wir Verbannen

- Bots: wenn die Erstellung automatisiert wirkt
- Werbung: wenn der Account für Werbezwecke erstellt wurde (Veröffentlichung in Timeline & Profiltext).

Wir werden auch alle Inhalte aus der öffentlichen Zeitleiste entfernen, die wir für hasserfüllt gegenüber bestimmten Einzelpersonen oder Gruppen halten. Inhalte, die dazu bestimmt sind, Schaden zu verursachen oder anzuregen. Diese Richtlinie ist absichtlich vage formuliert, weil wir nicht beabsichtigen "Rechtsanwalt" zu spielen, um zu sagen, was in Ordnung ist und was nicht. Eine gute Faustregel ist:

> Wenn du etwas sagen willst, von dem du denkst, dass es mit dieser Regel nicht übereinstimmt, solltest du es wahrscheinlich nicht sagen.

**Diese Richtlinien sind kein juristisches Dokument, sie sind hier, um dir einen Einblick in unsere Moderationsrichtlinien zu geben. Die endgültige Auslegung obliegt der Verwaltung von rollenspiel.social. Die Verwaltung behält sich das Recht vor, die Zugriffsberechtigungen eines Benutzers jederzeit und aus beliebigem Grund zu widerrufen, sofern dies nicht gesetzlich vorgeschrieben ist.**

---

## Berichterstattung / Moderationshinweise

Wenn du Inhalte siehst, von denen du glaubst, dass sie gegen diese Regeln verstoßen, melde diese bitte zur Überprüfung. Das ist essenziell für uns, da es unsere Arbeit erleichtert und diese Instanz zu einem angenehmeren Ort macht. Bitte beachte jedoch, dass wiederholte falsche Meldungen von Benutzern nicht erwünscht sind und als Feature-Missbrauch behandelt werden.

Falls du versehentlich gegen unsere Regeln verstoßen hast, könnten wir versuchen dich zu erreichen, also stelle bitte sicher, dass das Team von rollenspiel.social nicht blockiert wird.

Also bitte melde Verhalten, das dich stört. Das schließt Verhalten von anderen Instanzen mit ein - wir sind sehr gerne bereit, mit ignoranten Instanzen die Verbindung zu unterbrechen. Die Rohform sowie die Liste der blockierten Instanzen findest du auf unserem [Git-Repository](https://codeberg.org/RollenspielMonster/rollenspiel.social).

---

## Federation Status

rollenspiel.social glaubt nicht an Zensur. Fediverse ist ein Netzwerkdienst und die Aussetzung der Fediverse mit einem anderen Server wird nur als letztes Mittel durchgeführt, wenn Inhalte von einem anderen Server nach deutschem Recht Probleme verursachen können, oder bei technischen Schwierigkeiten, feindlichen oder bösartigen Aktivitäten. Wenn wir aus einem, oder mehreren dieser Gründe die Fediverse aussetzen oder eine andere Instanz zum Schweigen bringen müssen, wird dies [hier](https://codeberg.org/RollenspielMonster/rollenspiel.social/src/branch/master/blocked_instances.md) vermerkt. Haben wir deiner Meinung nach eine falsche Entscheidung getroffen? Kontaktiere uns!

---

## Meldung eines Problems

Für technische Probleme verwende die [Kontaktinformationen](https://rollenspiel.monster/kontakt/) des Administrators, ansonsten stehen folgende Optionen zur Verfügung:

- Wenn du ein Benutzer auf rollenspiel.social bist, kannst du die Funktion "Missbrauch melden" für den Beitrag nutzen, den du melden möchtest
- Wenn du kein Benutzer auf rollenspiel.social bist, oder wenn du ein Administrator eines anderen Servers bist, der eine Missbrauchsmeldung über einen unserer Benutzer erhalten hat, besprechen möchtest, verwende bitte folgende [Kontaktinformationen](https://rollenspiel.monster/kontakt/)

**Aus Datenschutzgründen werden wir keine Missbrauchsmeldungen mit dritten und/oder in öffentlichen Kanälen diskutieren.**

---

## Staff

- [@rkbw](https://rollenspiel.social/@rkbw) (Moderator)
- [@ZarJohn](https://rollenspiel.social/@zarjohn) (Moderator)
- [@Tealk](https://rollenspiel.social/@tealk) (Site Owner)

---

## Impressum & Datenschutzinformation

Hier gelangen Sie zu den [Nutzungsbedingungen](/about/more#richtlinien)
Hier gelangen Sie zum [Impressum](/terms#impressum)
Hier gelangen Sie zu den [Datenschutzinformation](/terms#datenschutzinformation)

---

## Funding

Unsere Webdienste und die dazugehörigen Server sind Spenden finanziert und werden durch unsere erstaunliche Community ermöglicht.

Wie ihr uns helfen könnt, findet ihr auf unserer [Unterstützungsseite](https://rollenspiel.monster/donate).

---
