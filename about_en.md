## Guidelines

On rollenspiel.social, our main guideline is:  
→ "Be nice to each other!"

> Unfortunately, this isn't always as simple as it sounds, so we've summarized a few points here to give you some insight into our moderation guidelines. The final interpretation is up to the administration of rollenspiel.social. The administration reserves the rights to delete toots or block an account if the issues persist.

Remember that there are real people on the other side of the screen and these people may have different feelings, opinions and experiences than you. Some of them may be tired or having a bad day.

Even if you don't to share someone's opinion you can act civil with them. If you can't act civil with someone, leave them alone.

If you don't want to interact with someone or don't want to see one of their posts, tools like mute and block functions are available to you.

If someone's posts make you angry or upset, take some time to calm down before responding. You could also try asking them to stop mentioning you if you no longer want to discuss a topic.

Remember that you can't control other people's behavior, but you are still responsible for your own. Even if you feel someone has been rude to you or someone else.

We do not allow abusive or disruptive behavior by or towards users on rollenspiel.social. This includes contacting people who have made it clear that they do not want to be contacted; as well as encouraging other users to contact that person on your behalf. Sharing another person's personal information without their consent.

Before you post something, think about whether this is something you would like to see in the public timeline (or for your mom or kids to see). There are visibility settings for which timelines your post will appear in. Also use the CW buttons to hide certain texts and images in your post.

If you publish mostly automated posts, please mark your account as bot. If there are more than five posts a day, publish your posts as "unlisted"; so that other posts can be found in the local timeline.

Please mark crossposts from Twitter as "unlisted" or, preferably, crosspost from Mastodon to Twitter.

The instance language is German, so we ask all users to post in German as well. Apart from that posts can also be published in English, posts or accounts (profile texts or links to websites) in other languages cannot be moderated by us, so we will remove them.

### This content on rollenspiel.social is not allowed.

- Sexual depictions of children
- Content that is illegal in Germany
- Discrimination against or advocacy of gender and sexual minorities
- Violent nationalistic propaganda, Nazi symbolism or promoting the ideology of National Socialism

### Content that will be removed if it appears in the public timeline without being flagged:

- Advertising
- Pornographic and/or sexual content
- Depictions of violence in words or pictures
- Religious & political content

> Flagging means the CW([content warnings](https://handbuch.rollenspiel.monster/mastodon/toots/content-warning.html)) function.

### Accounts we ban

- Bots: if the creation seems automated
- Advertising: if the account was created for advertising purposes (posting in timeline & profile text).

We will also remove any content from the public timeline that we deem hateful towards specific individuals or groups. Content that is intended to cause or incite harm. This policy is intentionally vague because we do not intend to play "lawyer" and tell you what is okay and what is not. A good rule of thumb is:

> If you're going to say something that you think is inconsistent with this rule, you probably shouldn't say it.

**These guidelines are not a legal document, they are here to give you some insight into our moderation guidelines. The final interpretation is up to the administration of rollenspiel.social. Administration reserves the rights to revoke a user's access privileges at any time, for any reason, unless required by law.**.

---

## Reporting / moderation tips

If you see content that you believe violates these rules, please report it for review. This is essential for us as it makes our job easier and makes this instance a more pleasant place to be. However, please note that repeated false reports from users are not welcome and will be treated as feature abuse.

If you accidentally violate our rules, we might try to reach you, so please make sure that the team of rollenspiel.social is not blocked.

So please report behavior that bothers you. This includes behavior from other instances - we are very happy to disconnect with ignorant instances. You can find the raw form as well as the list of blocked instances on our [git repository](https://codeberg.org/RollenspielMonster/rollenspiel.social).

---

## Federation Status

rollenspiel.social does not believe in censorship. The Fediverse is a network service and suspending Fediverse with another server is only done as a last resort when content from another server may cause problems with German law, or in case of technical difficulties, hostile or malicious activity. If we have to suspend federation or silence another instance for one, or more, of these reasons, it will be noted [here](https://codeberg.org/RollenspielMonster/rollenspiel.social/src/branch/master/blocked_instances.md). Do you think we made a wrong decision? Contact us!

---

## Reporting a problem

For technical issues use the administrator's [contact information](https://rollenspiel.monster/kontakt/), otherwise the following options are available:

- If you are a user of rollenspiel.social, you can use the "Report abuse" feature of the post you want to report.
- If you are not a user of rollenspiel.social, or if you are an administrator of another server who has received an abuse report about one of our users, please use the following [contact information](https://rollenspiel.monster/kontakt/) to discuss it.

**For privacy reasons, we will not discuss abuse reports with third parties and/or in public channels.**

---

## Staff

- [@rkbw](https://rollenspiel.social/@rkbw) (Moderator)
- [@ZarJohn](https://rollenspiel.social/@zarjohn) (Moderator)
- [@Tealk](https://rollenspiel.social/@tealk) (Site Owner)

---

## Imprint & Privacy Information

Here you get to the [terms of use](/about/more#guidelines)
Here you can access the [imprint](/terms#imprint)
This will take you to the [privacy information](/terms#data-protection-information)

---

## Funding

Our web services and associated servers are funded by donations and made possible by our amazing community.

Find out how you can help us on our [support page](https://rollenspiel.monster/donate).

---
