rollenspiel.social is a Mastodon instance for people into roleplaying games and is provided by the roleplaying community <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b>. We provide a place for roleplaying, pen & paper, tabletop, TCG and much more.
The primary instance language is German.
