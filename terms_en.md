Here you get to the [terms of use](/about/more#guidelines)
Here you can access the [imprint](/terms#imprint)
This will take you to the [privacy information](/terms#data-protection-information)

## Imprint

**Information according to § 5 TMG**

Daniel Buck  
Kirchenstr. 2  
92693 Eslarn  
phone: +49 156 78563777  
E-Mail: [webmaster@rollenspiel.social](mailto:webmaster@rollenspiel.social)

**Responsible for the content according to § 55 Abs. 2 RStV**

Daniel Buck  
Kirchenstr. 2  
92693 Eslarn

**Liability for contents**

As a provider I am responsible according to § 7 Abs. 1 TMG for my own content on this Mastodon instance according to the general laws. According to §§ 8 to 10 TMG I am not obligated to monitor transmitted or stored information or to investigate circumstances that indicate illegal activity. Obligations to remove or block the use of information under the general laws remain unaffected. However, liability in this regard is only possible from the point in time at which a concrete infringement of the law becomes known. Upon becoming aware of any such infringement, I will immediately take appropriate action.

**Liability for links**

This instance contains links to external websites, on which contents I have no influence. Therefore, I cannot take any responsibility for these external contents. The respective provider or operator of the sites is always responsible for the linked content. A permanent control of the linked pages is not reasonable without concrete evidence of a violation. If I become aware of any infringement, I will immediately take the necessary steps.

**Copyright**

The content created here by Mastodon users or me is subject to German copyright law. Duplication, processing, distribution, or any form of commercialization of such material beyond the scope of the copyright law shall require the prior written consent of its respective author or creator. Downloads and copies are only permitted for private, non-commercial use. Should you become aware of any copyright infringement, please inform me. If I become aware of any infringement, I will immediately take the necessary steps with regard to such content.

# Data protection information

In the following we inform you according to the EU General Data Protection Regulation (GDPR), applicable since May the 25th of 2018, about our handling of your personal data and what rights you have towards us.

## Person responsible

Responsible for the processing of personal data is:

Daniel Buck  
Phone: +49 156 78563777  
Email: [webmaster@rollenspiel.social](mailto:webmaster@rollenspiel.social)

## Applications and services

When using our offers, different types of personal data are processed differently. In the following, we will inform you about which applications and services process which information, which personal data are affected, what the purpose of the respective processing is, how long the data are stored and much more.

## Your rights

The data protection law grants you the following rights with regard to the processing of your personal data, the exact wording of which you can read in the linked articles of the GDPR:

- Right of access by the data subject pursuant to [Art. 15 GDPR](https://gdpr-info.eu/art-15-gdpr/)
- Right to rectification according to [Art. 16 GDPR](https://gdpr-info.eu/art-16-gdpr/)
- Right to erasure according to [Art. 17 GDPR](https://gdpr-info.eu/art-17-gdpr/)
- Right to restriction of processing pursuant to [Art. 18 GDPR](https://gdpr-info.eu/art-18-gdpr/)
- Notification obligation regarding rectification or erasure of personal data or restriction of processing pursuant to [Art. 19 GDPR](https://gdpr-info.eu/art-19-gdpr/)
- Right to data portability pursuant to [Art. 20 GDPR](https://gdpr-info.eu/art-20-gdpr/)
- Right to withdraw consent given in accordance with [Art. 7(3) GDPR](https://gdpr-info.eu/art-7-gdpr/)
- Right to object pursuant to [Art. 21GDPR](https://gdpr-info.eu/art-21-gdpr/)
- Right to lodge a complaint pursuant to [Art. 77 GDPR](https://gdpr-info.eu/art-77-gdpr/)

Please note that exercising your right to erasure, especially of your registration data, may mean that you can no longer use our service.

## Actuality of the data protection information

This data protection information can be adapted from time to time to organizational or technical changes and new legal circumstances. It is always valid in the current version published here.

## Processing of different types of personal data

When using our Mastodon instance, different types of personal data are processed differently. In the following, we will inform you about which types of processing exist, which personal data are affected in each case, what the purpose of the respective processing is, how long the data is stored in each case and much more.

### Calling the web application and browser access data.

**Purpose**: We provide the web application for your use, which, among other things, uses certain data, e.g. language information sent along by the browser, for appropriate display.

**Personal Data**: Transmitted are, among other things, depending on the browser used and its settings: date and time of access, if applicable, source/reference, which page redirected you to which one of ours, browser used, operating system used, language information, size of the browser window, IP address used.

**Storage period**: We do not store this data, it is only used when you access our web application.

**Legal basis**: To provide potential users with an overview of the web application and to provide registered users with the full application functionality, according to Art. 6 para. 1 lit. b) GDPR.

**Receiver**: Your browser access data can only be viewed by us as the operator and, if applicable, our provider.

**Third Country Transfer**: The data will not be stored in a third country.

### Registration and account data

**Purpose**: The account data is required for secure user registration, subsequent use of the application and to verify your identity.

**Personal Data**: Username, email address and password.

**Storage period**: Until your account is deleted. Deletion of the account will force the deletion of all posts and interactions.

**Legal basis**: Establishment of the user relationship between us as the operator of the application and you as the user of the Mastodon account, pursuant to Art. 6 para. 1 lit. b) GDPR.

**Receiver**: Your account data can only be viewed by us and, if available, our provider.

**Transfer to third parties**: The login data will not be shared with third parties, unless a law forces us to do so.

**Third Party Transfer**: Email address and password are not stored by us in a third party country outside the EU. The username is public worldwide.

### Optional profile information

**Purpose**: Voluntary worldwide publishing of more information about you.

**Personal Information**: Display name, biography (About Me), profile information fields, profile picture and cover photo, whom you follow and who follows you.

**Storage period**: Until deleted by you or deletion of the account.

**Legal basis**: Consent by voluntarily providing the data according to Art. 6 para. 1 lit. a) GDPR.

**Recipient**: Your profile information display name, biography, profile picture and cover picture are always public. Other information, such as people who follow you or whom you follow, may be restricted in your visibility by you.

**Transfer to third parties**: none.

**Third Party Transfer**: All profile information provided is publicly available worldwide. It may also appear on servers in third party countries through Federation and is subject to local jurisdiction there.

### Posts and Interactions

**Purpose**: Publishing own microblog posts (toots) and publishing (boosting) the posts of others, if desired, possibly even worldwide.

**Personal Data**: All your posts, including username, the attachments and other interactions (such as favorites, follows and reblogs), including all metadata, such as timestamps.

**Storage period**: Until deleted by you or deletion of the account.

**Legal basis**: Consent by publishing posts according to Art. 6 para. 1 lit. a) GDPR. When boosting the toots of others, you as the user of the account are responsible for ensuring that their privacy rights are respected.

**Receiver**: Your posts and interactions are in principle public, but can be restricted in visibility or promotion by the options you choose. The options are: public, public but not listed in the public timeline, only for followers or only to mentioned users.

**Sharing with third parties**: "Public in principle" means that this content is promoted to other servers worldwide, whose privacy and processing of this data we cannot control. Microblogging is in princible public, for private non-public communication other means must be used.

**Third Party Transfer**: All posts written are publicly available worldwide.

### Reporting of content and moderation.

**Purpose**: We moderate reported content for compliance with the Terms of Use and applicable laws.

**Personal Data**: All content, account data, and other information by you.

**Duration of Storage**: Reported posts will be flagged when reported until a moderator decides on further action. In case of violations of laws or terms of use, posts will be deleted promptly. In case of legal obligation, the above data may have to be saved as evidence and handed over to authorities.

**Legal basis**: In case of necessary surrender to authorities to comply with applicable laws according to Art. 6 para. 1 lit. c) GDPR or otherwise the legitimate interest according to Art. 6 para. 1 lit. f) GDPR to enable compliance with the terms of use.

**Receiver**: We as the operator incl. named moderators, if applicable.

**Transfer to third parties**: In case of violations of the law that have to be reported, all required evidence will be given to the investigating authorities. Otherwise, no data will be made available to third parties.

**Third Party Transfer**: None

### Cookies

**Purpose**: We use cookies to keep you logged in and save your settings for future visits.

**Personal Data**: A session cookie `_mastodon_session` to keep you logged in, a `_session_id` cookie to save you for later, and a cookie called `remember_user_token` to keep you as a user longer.

**Save duration**: Cookie `_mastodon_session` until you log out and end the browser session, `_session_id` and `remember_user_token` for one year.

**Legal basis**: Technical fulfillment of the user relationship between us as the operator of the application and you as the user of the Mastodon account, pursuant to Art. 6 para. 1 lit. b) GDPR.

**Receiver**: Only you, stored in the web browser

**Transmission to third parties**: None

**Third Party Transfer**: None
