# simple-toot

simple-toot is a shell script that sends out a predefined text message. rollenspiel.social uses it to welcome new members to the instance.

## What does the script do?

1. The user runs the script with a collection of Mastodon accounts to mention in the toot, i.e.
   ```bash
   ./simple-toot.sh "@test1 @test2 @test3"
   ```
2. (One time) The script registers a new app with the instance. The instance name is currently hardcoded in the script.
3. (One time) The script prompts the user to open up a URL in their browser to authorize the simple-toot to toot as them.
4. (One time) The script retrieves an access token from the server.
5. The script sends out a public toot with a predefined message. The accounts to mention which were given as a parameter in the first step are inserted into the message.

## Using the script in an automated pipeline

The goal is to automate the welcome message for new users.

Given a textfile with new account information, we can do the following:

```bash
grep '^@' user.txt | \
    xargs | \
    fold -w 143 -s | \
    xargs -I {} ./simple-toot.sh "{}"
```

Here's a step-by-step breakdown.

1. We use `grep` to find lines that contain a username. Based on the current format of our new account information, these are lines that start with '@'.
2. The output is piped into `xargs` to remove any trailing whitespaces and put all new accounts on a single line.
3. `fold` splits the line on whole words with the `-w` switch. This ensures that we don't go over the character limit for our welcome toot.
4. The final `xargs` takes the wrapped lines and hands them over to the simple-toot script as the input argument. Depending on the number of new accounts this will send out one or more welcome toots.
