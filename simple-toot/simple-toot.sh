#!/usr/bin/env bash

# Based on a tutorial found at
# https://leancrew.com/all-this/2018/08/autotooting-with-mastodon/
# although the OAuth API endpoint had to be adjusted slightly according to
# https://docs.joinmastodon.org/methods/apps/oauth/#obtain-a-token

set -o errexit
set -o pipefail
set -o nounset

NEW_ACCOUNTS="${1}"

TOOL_NAME="simple-toot"
SERVER="rollenspiel.social"
TEXT=":rm_love: willkommen auf unserer Instanz und im #fediverse

${NEW_ACCOUNTS}

#neuhier

Falls ihr Fragen habt einfach raus damit, im #fediverse sind viele nette Menschen, die euch gerne helfen werden.
Ansonsten könnt ihr euch gerne einmal in unserem Handbuch umsehen: https://handbuch.rollenspiel.monster

Neuigkeiten findet ihr unter diesem Account oder für #RollenspielMonster auf @rm_news."

CONFIG_PATH="${XDG_CONFIG_HOME:-${HOME}/.config}"
TOOL_CONFIG_PATH="${CONFIG_PATH}/${TOOL_NAME}"
ACCESS_TOKEN_FILE="${TOOL_CONFIG_PATH}/access_token"
CLIENT_ID_FILE="${TOOL_CONFIG_PATH}/client_id"
CLIENT_SECRET_FILE="${TOOL_CONFIG_PATH}/client_secret"

CLIENT_ID=""
CLIENT_SECRET=""

get_value_from_json () {
    JSON=$1
    PROPERTY=$2
    echo "${JSON}" | \
        python3 -c "import sys, json; print(json.load(sys.stdin)['${PROPERTY}'])"
}

get_client_id_and_secret () {
    if [[ ! -f "${CLIENT_ID_FILE}"
        || ! -f "${CLIENT_SECRET_FILE}" ]]
    then
        APP_RESPONSE=$(curl -s -X POST \
            -F "client_name=${TOOL_NAME}" \
            -F "redirect_uris=urn:ietf:wg:oauth:2.0:oob" \
            -F "scopes=write" \
            -F "website=https://rollenspiel.social" \
            "https://${SERVER}/api/v1/apps")

        get_value_from_json "${APP_RESPONSE}" "client_id" > \
            "${CLIENT_ID_FILE}"
        chmod 600 "${CLIENT_ID_FILE}"

        get_value_from_json "${APP_RESPONSE}" "client_secret" > \
            "${CLIENT_SECRET_FILE}"
        chmod 600 "${CLIENT_SECRET_FILE}"

    fi
    CLIENT_ID=$(cat "${CLIENT_ID_FILE}")
    CLIENT_SECRET=$(cat "${CLIENT_SECRET_FILE}")
}

request_authorization () {
    # https://mastodon.example/oauth/authorize
    echo "${TOOL_NAME} is not yet authorized to use your account."
    echo "Please visit the following URL in your browser to authorize ${TOOL_NAME} to toot as your user:"
    echo "https://${SERVER}/oauth/authorize?response_type=code&client_id=${CLIENT_ID}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=write"

    read -p "Enter the authorization code: " AUTH_CODE
    # Get OAuth token, requires Python to parse the JSON output
    # https://stackoverflow.com/a/1955555
    AUTH_RESPONSE=$(curl -s -X POST \
        -F "client_id=${CLIENT_ID}" \
        -F "client_secret=${CLIENT_SECRET}" \
        -F "scope=write" \
        -F "grant_type=authorization_code"\
        -F "code=${AUTH_CODE}" \
        -F "redirect_uri=urn:ietf:wg:oauth:2.0:oob" \
        "https://${SERVER}/oauth/token")

    get_value_from_json "${AUTH_RESPONSE}" "access_token" > \
        "${ACCESS_TOKEN_FILE}"

    chmod 600 "${ACCESS_TOKEN_FILE}"
}

send_toot () {
    ACCESS_TOKEN=$(cat "${ACCESS_TOKEN_FILE}")

    curl -s -X POST \
        -H "Authorization: Bearer ${ACCESS_TOKEN}" \
        -F "status=${TEXT}" \
	-F "visibility=direct" \
        "https://${SERVER}/api/v1/statuses"
}

main () {
    mkdir -p "${TOOL_CONFIG_PATH}"

    if [[ ! -f "${ACCESS_TOKEN_FILE}" ]]
    then
        get_client_id_and_secret
        request_authorization
    fi

    send_toot
}

main
