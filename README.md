# Deutsch

## Allgemeines

Dieses Repository enthält Informationen über [rollenspiel.social](https://rollenspiel.social), eine Mastodon-Instanz für und von der Rollenspiel-Community [RollenspielMonster](https://rollenspiel.monster "RollenspielMonster - Deine Rollenspiel Community").

## Die einzelnen Dateien

Du kannst unsere aktuelle block/silence-Liste in [blocked_instances.md](blocked_instances.md) und unsere aktuellen Server-Regeln in [about.md](about.md) einsehen. Unter [terms.md](terms.md) seht ihr die Datenschutzerklärung.
Die Markdown dateiern werden aus den html Dateien erstellt, also kann die Darstellung ein wenig von dem Orginal abweichen.

## Kontakt

Wenn dich etwas an unserer Instanz stört, kannst du dich an [uns](https://rollenspiel.monster/kontakt/) wenden. Wenn du möchtest das rollenspiel.social einen Domainblock hinzufügten, dann erstelle bitte ein [Issue](https://codeberg.org/RollenspielMonster/rollenspiel.social/issues).

## Benutzung

Ihr könnt dieses Repository gerne als Vorlage für euere eigene Instanz benutzen.

## Versionierung

Neuerungen werde ich immer mit [Tags](https://codeberg.org/RollenspielMonster/rollenspiel.social/tags) Versionieren, diese setzt sich aus Jahreszahl(letzte 2 Stellen) und Monat zusammen. Es gibt eine Ausnahme, wenn Tippfehler oder Rechtschreibung korrigiert wird; dann kommt eine weitere stelle hinzu.

# English

## General

This repository contains information about [rollenspiel.social](https://rollenspiel.social), a Mastodon instance for and by the roleplay community [RollenspielMonster](https://rollenspiel.monster "RollenspielMonster - Your Roleplay Community").

## The individual files

You can see our current block/silence list in [blocked_instances.md](blocked_instances.md) and our current server rules in [about.md](about.md). Under [terms.md](terms.md) you can see the privacy policy.
The markdown files are created from the html files, so the appearance may differ slightly from the original.

## Contact us

If something about our instance bothers you, you can contact [us](https://rollenspiel.monster/kontakt/). If you want rollenspiel.social to add a domain block, please create an [issue](https://codeberg.org/RollenspielMonster/rollenspiel.social/issues).

## Usage

You are welcome to use this repository as a template for your own instance.

## Versioning

I will always version updates with [Tags](https://codeberg.org/RollenspielMonster/rollenspiel.social/tags), this is composed of year(last 2 digits) and month. There is an exception when correcting typos or spelling; then another digit is added.

---

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Die hier verfassten Texte</span> von [rollenspiel.social](https://codeberg.org/RollenspielMonster/rollenspiel.social) sind lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).  
Beruht auf dem Werk unter [https://github.com/chaossocial](https://github.com/chaossocial) und [https://legal.social/terms](https://legal.social/terms).
